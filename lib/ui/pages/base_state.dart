import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture_app/blocs/provider/bloc.dart';
import 'package:flutter_clean_architecture_app/blocs/provider/provider.dart';

abstract class BaseState<T extends StatefulWidget, K extends Bloc>
    extends State<T> {
  K bloc;

  @override
  void initState() {
    super.initState();
    bloc = Provider.of<K>(getBlocInstance);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    Provider.dispose<K>();
    super.dispose();
  }

  K getBlocInstance();
}
