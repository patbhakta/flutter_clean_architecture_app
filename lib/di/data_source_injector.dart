import 'package:data/security_repository.dart';
import 'package:flutter_clean_architecture_app/data_source/api/security_api.dart';
import 'package:flutter_clean_architecture_app/data_source/database/security_db.dart';
import 'package:flutter_clean_architecture_app/utils/connectivity/my_connectivity.dart';
import 'package:flutter_clean_architecture_app/utils/connectivity/my_connectivity_adapter.dart';

class DataSourceInjector {
  static DataSourceInjector _singleton;
  MyConnectivity _myConnectivity;

  factory DataSourceInjector() {
    if (_singleton == null) {
      _singleton = DataSourceInjector._();
    }
    return _singleton;
  }

  DataSourceInjector._() {
    _myConnectivity = MyConnectivityAdapter();
  }

  SecurityApi provideSecurityApiSource() {
    return SecurityApiAdapter();
  }

  SecurityDb provideSecurityDbSource() {
    return SecurityDbAdapter();
  }
}
