import 'package:data/security_repository.dart';
import 'package:domain/secutiry_use_case.dart';
import 'package:flutter_clean_architecture_app/di/data_source_injector.dart';

class RepositoryInjector {
  static RepositoryInjector _singleton;

  factory RepositoryInjector() {
    if (_singleton == null) {
      _singleton = RepositoryInjector._();
    }
    return _singleton;
  }

  RepositoryInjector._();

  SecurityRepository provideSecurityRepository() {
    return SecurityRepositoryAdapter(
      DataSourceInjector().provideSecurityApiSource(),
      DataSourceInjector().provideSecurityDbSource(),
    );
  }
}
